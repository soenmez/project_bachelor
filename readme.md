# Bachelor thesis

In this repository you will find all materials used for the thesis:

> Finding evidence for the pain-awareness of non-human animals with information extraction

by Aysu Soenmez.


<details open="open">
<summary>Table of contents:</summary>

- [Annotations](#annotations)
- [Datasets](#dataset)
    - [Document level](#document-level)
    - [Unary relation extraction](#unary-relation-extraction)
- [Training](#training)
    - [Logistic regression](#logistic-regression)
    - [BioLinkBERT](#biolinkbert)
    - [DistilRoBERTa](#distilroberta)
- [Classifier](#classifier)
- [Experiments](#experiments)

</details>

## Annotations

`/brat_annotations` contains all the manually annotated abstracts in the format `$PMID.ann`.  
It is possible to upload this folder into a [brat](https://github.com/nlplab/brat) installation to visualize and edit the annotations. This folder also contains the background knowledge used in the annotation process. 

## Datasets

`/datasets` contains the notebooks used to generate the final datasets used to train the models.

### Document level

The first notebook `dataset_doc_level.ipynb` generates the datasets used for the [logistic regression model](#logistic-regression) using on the whole dataset for document-level classification `dataset-doc-level.csv`.  

For each criterion it pulls an equal number of negatives and puts them in a separate dataset.

The resulting datasets can be found at [Huggingface](https://huggingface.co/datasets/aysusoenmez/awareness_dataset_doc-level).

### Unary relation extraction

The second notebook `dataset_unaryRE.ipynb` instead splits the same documents into individual species and tags each species mention with `[START]` and `[END]` using the whole dataset for document-level unary RE `dataset_unaryRE.csv`. It also pulls 10 times the negatives for each criterion. It is used for both our [BioLinkBERT](#biolinkbert) and [DistilRoBERTa](#distilroberta) models.  

The resulting datasets can be found at [Huggingface](https://huggingface.co/datasets/aysusoenmez/awareness_dataset).


## Training

`/training` contains the notebooks used to train the final models

### Logistic regression

`LogisticRegression.ipynb` uses the scikit-learn library to run a logistic regression training on the [Document level](#document-level) datasets.

### BioLinkBERT

`BioLinkBERT.ipynb` uses the huggingface trainer library to train binary classification models for each criterion on the [Unary relation datasets](#unary-relation-extraction).

The resuling models can be found at [Huggingface](https://huggingface.co/aysusoenmez/):

- [Criterion 1](https://huggingface.co/aysusoenmez/criterion_1)
- [Criterion 2](https://huggingface.co/aysusoenmez/criterion_2)
- [Criterion 3](https://huggingface.co/aysusoenmez/criterion_3)
- [Criterion 4](https://huggingface.co/aysusoenmez/criterion_4)
- [Criterion 7](https://huggingface.co/aysusoenmez/criterion_7)

### DistilRoBERTa

`DistilRoBERTa.ipynb` uses the huggingface trainer library to train binary classification models for each criterion on the [Unary relation datasets](#unary-relation-extraction).


## Classifier

`/classifier/classifier.py` is python CLI used to classify any number of PubMed IDs.

### Usage

`./classifier.py pubmed 24981559 20863441 24981559 35858073`

This will pull all of the specified PMIDs using the PubTator API and apply the [unary relation extraction preproccessing](#unary-relation-extraction). It will then pass each through our five [BioLinkBERT based models](#biolinkbert). 

### Output

```
Species:        9606
PMID:           35858073
Criterion 1:    True
Criterion 2:    False
Criterion 3:    True
Criterion 4:    True
Criterion 7:    False
```


The CLI will give us a human readable output per default, but can optionally return `json` or `yaml` for further processing using the `-f` flag. Example:

`./classifier.py -f json pubmed 24981559`

```json
[{
    "text": "text cut for preview purposes",
    "species": "6669",
    "pmid": "24981559",
    "predictions": {"1": true, "2": false, "3": false, "4": false, "7": false}
}]
```
### Experiments
\Experiments contains `cosine_sim.ipynb` which computes the cosine similarity between the train and test sets of the folds used for training the BioLinkBERT and the DistilRoBERTa model
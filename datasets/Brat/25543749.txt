A new species of Spongilla-fly from Western Africa (Neuroptera: Sisyridae).
A new species of spongilla-fly (Neuropterida, Neuroptera, Sisyridae: Sisyra) is described from Western Africa (Guinea and Ivory Coast). This new Sisyra species differs from all other known African species both in its morphology and genitalia, and it seems to be most closely related to a species in Thailand. 

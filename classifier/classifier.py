#!/usr/bin/env python3

import argparse
from collections import defaultdict
import re
import sys
from bioc import biocxml
import requests
from transformers import pipeline
import json
import yaml


def predict(data):
    # Pass data through the classifier pipeline:
    classifiers = {}
    for criterion in [1, 2, 3, 4, 7]:
        classifiers[criterion] = pipeline(
            model=f"aysusoenmez/criterion_{criterion}")

    text = []
    for d in data:
        d["predictions"] = {}
        text.append(d["text"])

    for criterion, classifier in classifiers.items():
        results = classifier(text, batch_size=8)
        for i, r in enumerate(results):
            prediction = False
            if r["label"] == "LABEL_1":
                prediction = True
            data[i]["predictions"][criterion] = prediction

    return data


def fmt(predictions):
    # Format the predictions:
    for p in predictions:
        if "species" in p:
            print(f"Species:\t{p['species']}")
        if "pmid" in p:
            print(f"PMID:\t\t{p['pmid']}")
        if not "species" in p and not "pmid" in p:
            print(f"Text:\t\t{p['text']}")
        for criterion, prediction in p["predictions"].items():
            print(f"Criterion {criterion}:\t{prediction}")
        print()


def batch(iterable, n=1):
    # Batch a list into chunks of size n
    l = len(iterable)
    for ndx in range(0, l, n):
        yield iterable[ndx:min(ndx + n, l)]


def get_documents(ids):
    # Fetch documents from PubMed in batches of 100 and return a list of documents
    documents = []
    try:
        for b in batch(ids, 100):
            pmids = ",".join([str(i) for i in b])
            res = requests.get(
                f"https://www.ncbi.nlm.nih.gov/research/pubtator-api/publications/export/biocxml?pmids={pmids}&concepts=species")
            response = res.text
            collection = biocxml.loads(response)
            documents += collection.documents
    except Exception as e:
        print("Error fetching documents!", e, file=sys.stderr)
        sys.exit(1)

    return documents


def preprocess(documents):
    # Split the PubMed documents into individual species and add start/end tokens for the classifier
    data = []
    for document in documents:
        for passage in document.passages:
            text = re.sub("\n\s*", " ", passage.text)
            d = defaultdict(lambda: text)
            for annotation in reversed(passage.annotations):
                if annotation.infons["type"] != "Species":
                    continue
                t = d[annotation.infons["identifier"]]
                location = annotation.locations[0]
                start = location.offset - passage.offset
                end = location.offset + location.length - passage.offset
                t = f'{t[:start]}[START]{annotation.text}[END]{t[end:]}'
                d[annotation.infons["identifier"]] = t
            for species, text in d.items():
                data.append(
                    {"text": text, "species": species, "pmid": document.id})
    return data


def predict_pubmed(ids):
    # Fetch documents from PubMed, preprocess them, and pass them through the classifier pipeline
    documents = get_documents(ids)
    processed = preprocess(documents)
    return predict(processed)


def predict_json(j):
    # Validate the JSON and pass it through the classifier pipeline
    try:
        l = json.loads(j)
    except Exception as e:
        print("Invalid JSON!", e, file=sys.stderr)
        sys.exit(1)

    if type(l) != list:
        print("JSON must be a list")
        sys.exit(1)
    if len(l) == 0:
        print("JSON list must not be empty")
        sys.exit(1)

    for d in l:
        if type(d) != dict:
            print("JSON list must contain only objects")
            sys.exit(1)
        if not "text" in d:
            print("JSON objects must contain 'text' field")
            sys.exit(1)

    return predict(l)


if __name__ == "__main__":
    # Parse command line arguments

    parser = argparse.ArgumentParser(
        prog='Awareness Classifier',
        description='Classify awareness criteria given a list of abstracts',
        epilog='Usage: %(prog)s [options]'
    )

    parser.add_argument(
        '-f', '--format', choices=['text', 'json', 'yaml'], default='text')

    subparsers = parser.add_subparsers(
        dest='op')

    subparsers.required = True

    pm = subparsers.add_parser('pubmed', help='Fetch abstracts from PubMed')
    pm.add_argument('pmid', nargs="+", type=int, help='PubMed ID')

    j = subparsers.add_parser('json', help='Fetch abstracts from PubMed')
    j.add_argument('json', type=str, help='Manually formatted JSON list')

    args = parser.parse_args()

    predictions = []
    if args.op == "pubmed":
        predictions = predict_pubmed(args.pmid)
    elif args.op == "json":
        predictions = predict_json(args.json)

    if args.format == "json":
        print(json.dumps(predictions))
    elif args.format == "yaml":
        print(yaml.dump(predictions))
    else:
        fmt(predictions)
